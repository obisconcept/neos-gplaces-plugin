# Neos CMS - Google Places Plugin
A small node type based Google Maps plugin for Neos CMS using the Places API for a category based proximity search.

## Installation
Add the package in your site package composer.json
```
"require": {
    "obisconcept/neos-gplaces-plugin": "~1.0"
}
```

## Version History

### Changes in 1.0.0
- First version of the Neos CMS Google Places plugin